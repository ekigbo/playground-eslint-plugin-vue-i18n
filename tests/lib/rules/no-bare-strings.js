/**
 * @fileoverview Enforce externalisation of translateable strings in a .vue file
 * @author theatlasroom
 */
'use strict'

// ------------------------------------------------------------------------------
// Requirements
// ------------------------------------------------------------------------------

const rule = require('../../../lib/rules/no-bare-strings')
const RuleTester = require('eslint').RuleTester

// ------------------------------------------------------------------------------
// Tests
// ------------------------------------------------------------------------------

const ruleTester = new RuleTester({
  parser: 'vue-eslint-parser',
  parserOptions: { ecmaVersion: 2015 }
})
ruleTester.run('no-bare-strings', rule, {
  valid: [
    {
      filename: 'test.vue',
      code: '<template><div>{{ _("test") }}</div></template>'
    },

    {
      filename: 'test.vue',
      code: '<template><div>{{ test }}</div></template>'
    },
    {
      filename: 'test.vue',
      code: '<template><div><h1>{{ _("test") }}</h1></div></template>'
    },
    {
      filename: 'test.vue',
      code: '<template><div><h1>{{ test }}</h1></div></template>'
    },
    {
      filename: 'test.vue',
      code: '<template><h1 v-html="test"></template>'
    },
    {
      filename: 'test.vue',
      code: '<template><h1 :text="testB"/></template>'
    },
    {
      filename: 'test.vue',
      code: '<template><div aria-label="testB"/></template>'
    },
    {
      filename: 'test.vue',
      code: '<template><div aria-label="\'testB\'"/></template>'
    },
    {
      filename: 'test.vue',
      code:
        '<template><div><input type="text" placeholder="This is placeholder text" /></div></template>'
    },
    {
      filename: 'test.vue',
      code:
        '<template><img src="/some/image/path" alt="Amazing image" /></template>'
    },
    {
      filename: 'test.vue',
      code:
        '<template><abbr title="Hypertext Markup Language">{{ __("HTML") }}</abbr></template>'
    },
    {
      filename: 'test.vue',
      code:
        '<template><div :class="`js-${scope}-tab-${tab.scope}`"></div></template>'
    },
    {
      filename: 'test.vue',
      code: '<template><p>&times;</p></template>'
    },
    {
      filename: 'test.vue',
      code: '<template><p>→</p></template>'
    },
    {
      filename: 'test.vue',
      code: '<template><p>·</p></template>'
    },
    {
      filename: 'test.vue',
      code: '<template><p>!</p></template>'
    },
    {
      filename: 'test.vue',
      code: '<template><p>&middot;</p></template>'
    },
    {
      filename: 'test.vue',
      code: '<template><p>×</p></template>'
    },
    {
      filename: 'test.vue',
      code: '<template><p>--<p></template>'
    }
  ],

  invalid: [
    {
      filename: 'test.vue',
      code: '<template><div>"test"</div></template>',
      errors: ['Content should be marked for translation']
    },
    {
      filename: 'test.vue',
      code: "<template><div>'test'</div></template>",
      errors: ['Content should be marked for translation']
    },
    {
      filename: 'test.vue',
      code: '<template><div>`test`</div></template>',
      errors: ['Content should be marked for translation']
    },
    {
      filename: 'test.vue',
      code: '<template><div>test</div></template>',
      errors: ['Content should be marked for translation']
    },
    {
      filename: 'test.vue',
      code: '<template><div>{{ "test" }}</div></template>',
      errors: ['Content should be marked for translation']
    },
    {
      filename: 'test.vue',
      code: "<template><div>{{ 'test' }}</div></template>",
      errors: ['Content should be marked for translation']
    },
    {
      filename: 'test.vue',
      code: '<template><div>{{ `test` }}</div></template>',
      errors: ['Content should be marked for translation']
    },
    {
      filename: 'test.vue',
      code: '<template><div><h1>test</h1></div></template>',
      errors: ['Content should be marked for translation']
    },
    {
      filename: 'test.vue',
      code: '<template><div>{{ `test ${something}` }}</div></template>',
      errors: ['Content should be marked for translation']
    },
    // TODO: double check this syntax below
    {
      filename: 'test.vue',
      code: '<template><div>{{ `test %{"something"}` }}</div></template>',
      errors: ['Content should be marked for translation']
    }
  ]
})
