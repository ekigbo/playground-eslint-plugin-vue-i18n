const CALL_EXPRESSION = 'CallExpression'
const V_ATTRIBUTE = 'VAttribute'
const V_DIRECTIVE_KEY = 'VDirectiveKey'
const V_EXPRESSION_CONTAINER = 'VExpressionContainer'
const V_LITERAL = 'VLiteral'
const V_TEXT = 'VText'
const V_ELEMENT = 'VElement'
const TEMPLATE_LITERAL = 'TemplateLiteral'
const LITERAL = 'Literal'

const NODE_TYPES = {
  CALL_EXPRESSION,
  V_ATTRIBUTE,
  V_DIRECTIVE_KEY,
  V_EXPRESSION_CONTAINER,
  V_LITERAL,
  V_TEXT,
  V_ELEMENT,
  TEMPLATE_LITERAL,
  LITERAL
}

/**
 * Check that a node is a `VLiteral type`
 * @param {ASTNode} The node to type check
 * @returns {boolean} `true` if the node is a VLiteral type
 */
function isVLiteral (node) {
  return node.type === NODE_TYPES.V_LITERAL
}

/**
 * Select the correct fix function to apply to our node
 * @param {ASTNode} 'type' property is the node type ['VText','VLiteral...] 'value' the string to be fixed
 * @returns {array} In form [prefix, postfix] with the content to prepend and append to the string
 */
function selectFixFunction ({ type, value }) {
  // TODO: needs tests
  // TODO: these are gitlab specific fixes...
  // TODO: Need a config for the default externalization function to use
  // TODO: probably need withExpressionWithQuotes....
  const noQuotes = ['__(', ')']
  const withQuotes = ['__("', '")']
  const withExpression = ['{{ __(', ') }}']
  const withQuotesExpression = ['{{ __("', '") }}']

  const hasQuotes = isWrappedWithQuotes(value)

  // TODO: double check this is still needed
  if (type === NODE_TYPES.V_TEXT) {
    return hasQuotes ? withExpression : withQuotesExpression
  } else {
    return hasQuotes ? noQuotes : withQuotes
  }
}

/**
 * Check if the string starts and ends with a valid quote symbol
 * @param {string} The string to check
 * @returns {boolean} True if wrapped in quotes
 */
function isWrappedWithQuotes (str) {
  const len = str.length
  const quotes = ["'", '"']
  return (
    quotes.indexOf(str.charAt(0)) > -1 &&
    quotes.indexOf(str.charAt(len - 1)) > -1
  )
}

/**
 * Removes new line characters and strips trailing spaces
 * @param {string} The string to clean
 * @returns {string} The cleaned string
 */
function removeNewLines (str = '') {
  if (!str) return ''
  return str.replace('\n', '').trim()
}

/**
 * Replace multiple whitespaces with single whitespace
 * @param {string} The string to check
 * @returns {string} The string with excessive whitespaces removed
 */
function clearSplitLines (str) {
  return str.replace(/(\s)+/g, ' ')
}

/**
 * Extracts the contents of a TemplateLiteral node
 * @param {ASTNode} The node to extract text from
 * @returns {string} The extracted text
 */
function extractTemplateLiteralContent (node) {
  const quasis = node.quasis || []
  return removeNewLines(quasis.map(n => n.value.raw).join(''))
}

/**
 * Extracts the contents of a TemplateLiteral node
 * @param {ASTNode} The node to check
 * @returns {string} Text content of the node
 */
function getNodeValue (node) {
  const { type } = node
  switch (type) {
    case TEMPLATE_LITERAL:
      return extractTemplateLiteralContent(node)
    case V_EXPRESSION_CONTAINER:
      return node.expression.value
    case V_LITERAL:
    case V_TEXT:
      return node.value
    default:
      return null
  }
}

/**
 * Extracts the name of an attribute
 * @param {ASTNode} The node to check
 * @returns {string} Text content of the node
 */
function getAttributeName (node) {
  const { type } = node
  switch (type) {
    case V_ATTRIBUTE:
      return node.key.name
    default:
      return null
  }
}

module.exports = {
  NODE_TYPES,
  clearSplitLines,
  isVLiteral,
  removeNewLines,
  selectFixFunction,
  getNodeValue,
  getAttributeName
}
