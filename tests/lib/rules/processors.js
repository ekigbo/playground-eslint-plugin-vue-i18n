/**
 * @author kazuya kawaguchi (a.k.a. kazupon)
 */
'use strict'

const fs = require('fs')
const { join } = require('path')
const assert = require('assert')
const { cleanUpG2MEscaping, getLocaleMessages } = require(join(
  __dirname,
  '../../../',
  'lib/utils/index'
))

describe('Processors', () => {
  const base = join(__dirname, '../../')

  describe('json', () => {
    it('will load .json locale file', () => {
      const translatedFile = join(base, 'fixtures/processors/json/valid.json')
      const rawFile = join(base, 'fixtures/processors/json/en.json')
      fs.readFile(translatedFile, 'utf8', (_, valid) => {
        const [res] = getLocaleMessages(rawFile)
        const { messages } = res
        assert.deepEqual(messages, JSON.parse(valid))
      })
    })
  })

  describe('po', () => {
    it('will load .po locale file', () => {
      const data = [
        {
          po: join(base, 'fixtures/processors/po/en.po'),
          json: join(base, 'fixtures/processors/po/valid.json')
        },
        {
          po: join(base, 'fixtures/processors/po/simple.po'),
          json: join(base, 'fixtures/processors/po/simple.json')
        }
      ]
      for (const index in data) {
        const { po, json } = data[index]
        fs.readFile(json, 'utf8', (_, valid) => {
          const [res] = getLocaleMessages(po)
          const { messages } = res
          assert.deepEqual(
            cleanUpG2MEscaping({ data: messages }),
            JSON.parse(valid)
          )
        })
      }
    })

    it('will load .pot locale file', () => {
      const poFile = join(base, 'fixtures/processors/pot/template.pot')
      const jsonFile = join(base, 'fixtures/processors/pot/template.json')
      fs.readFile(jsonFile, 'utf8', (_, valid) => {
        const [res] = getLocaleMessages(poFile)
        const { messages } = res
        assert.deepEqual(
          cleanUpG2MEscaping({ data: messages }),
          JSON.parse(valid)
        )
      })
    })
  })
})
