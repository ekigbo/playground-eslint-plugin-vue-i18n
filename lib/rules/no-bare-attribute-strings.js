/**
 * @fileoverview enforce no bare attribute strings in vue templates
 * @author theatlasroom
 */
'use strict'

// ------------------------------------------------------------------------------
// Requirements
// ------------------------------------------------------------------------------

const { defineTemplateBodyVisitor } = require('../utils/index')
const {
  // selectFixFunction,
  clearSplitLines,
  removeNewLines,
  isVLiteral,
  getAttributeName
} = require('../utils/rule-utils')
// ------------------------------------------------------------------------------
//   Helpers
// ------------------------------------------------------------------------------
const DEFAULT_ATTRIBUTES = [
  'alt',
  'placeholder',
  'aria-label',
  'aria-placeholder',
  'aria-roledescription',
  'aria-valuetext'
  // NOTE: 'title' is excluded: https://html.spec.whatwg.org/multipage/dom.html#the-title-attribute
]

/**
 * Check for attributes that might contain bare strings that get displayed
 * @param {string} The node to check
 * @returns {boolean} True if its translateable
 */
function isTranslatableAttribute (attributes = [], attr) {
  return (
    attributes.length && attr && attributes.indexOf(removeNewLines(attr)) >= 0
  )
}

/**
 * Check for strings we should be ignoring
 * @param {Object} The eslint context object
 * @param {ASTNode} The node we are reporting on
 */
function report (context, node) {
  // TODO: cleanup these types into obj
  const { loc, value: attributeContent = null } = node
  context.report({
    loc: loc,
    message: 'Attribute value should be marked for translation',
    fix: fixer => {
      if (!attributeContent || !attributeContent.value) {
        return null
      }

      // externalize the node content
      // const wrapper = selectFixFunction(attributeContent)
      const wrapper = ['"__(`', '`)"']
      const linted = [
        wrapper[0],
        clearSplitLines(removeNewLines(attributeContent.value)),
        wrapper[1]
      ]
      return [
        // convert the attribute to a binding
        fixer.insertTextBefore(node, ':'),
        // externalize the node content
        fixer.replaceText(attributeContent, linted.join(''))
      ]
    }
  })
}

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = {
  meta: {
    type: 'error',
    docs: {
      description: 'enforce no bare strings in attributes',
      category: 'i18n',
      url: 'https://eslint.vuejs.org/rules/no-bare-attribute-strings.html'
    },
    fixable: 'code',
    schema: [
      {
        type: 'object',
        properties: {
          attributes: {
            type: 'array',
            items: { type: 'string' },
            default: DEFAULT_ATTRIBUTES
          }
        },
        additionalProperties: false
      }
    ]
  },

  create (context) {
    // ----------------------------------------------------------------------
    // Public
    // ----------------------------------------------------------------------
    // VNode = VAttribute | VDirective | VDirectiveKey | VDocumentFragment | VElement | VEndTag | VExpressionContainer | VIdentifier | VLiteral | VStartTag | VText
    return defineTemplateBodyVisitor(context, {
      // TODO: should we ignore v-text? or are there other rules for this (double check v-html)
      VAttribute (node) {
        const { options } = context

        const attributes = options.length
          ? options[0].attributes
          : DEFAULT_ATTRIBUTES
        const { directive = false, value = '' } = node

        const attributeName = getAttributeName(node)
        if (
          directive ||
          !isTranslatableAttribute(attributes, attributeName) ||
          !isVLiteral(value)
        ) {
          return
        }
        report(context, node)
      }
    })
  }
}
