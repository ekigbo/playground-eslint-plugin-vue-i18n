/**
 * @fileoverview enforce no bare strings attributes in vue templates
 * @author theatlasroom
 */
'use strict'

// ------------------------------------------------------------------------------
// Requirements
// ------------------------------------------------------------------------------

const rule = require('../../../lib/rules/no-bare-attribute-strings')
const RuleTester = require('eslint').RuleTester

// ------------------------------------------------------------------------------
// Tests
// ------------------------------------------------------------------------------

const ruleTester = new RuleTester({
  parser: 'vue-eslint-parser',
  parserOptions: { ecmaVersion: 2015 }
})

ruleTester.run('no-bare-attribute-strings', rule, {
  valid: [
    {
      filename: 'test.vue',
      code:
        '<template><div :class="`js-${scope}-tab-${tab.scope}`"></div></template>'
    },
    {
      filename: 'test.vue',
      code:
        '<template><p title="Hypertext Markup Language">{{ __("HTML") }}</p></template>'
    }
  ],

  invalid: [
    {
      filename: 'test.vue',
      code:
        '<template><input type="text" placeholder="This is placeholder text" /></template>',
      errors: ['Attribute value should be marked for translation']
    },
    {
      filename: 'test.vue',
      code:
        '<template><img src="/some/image/path" alt="Amazing image" /></template>',
      errors: ['Attribute value should be marked for translation']
    },
    {
      filename: 'test.vue',
      code:
        '<template><div aria-label="test this string">{{ "COOL" }}></div></template>',
      errors: ['Attribute value should be marked for translation']
    },
    {
      filename: 'test.vue',
      code:
        '<template><div aria-placeholder="test this string">{{ "COOL" }}></div></template>',
      errors: ['Attribute value should be marked for translation']
    },
    {
      filename: 'test.vue',
      code:
        '<template><div aria-valuetext="test this string">{{ "COOL" }}></div></template>',
      errors: ['Attribute value should be marked for translation']
    },
    {
      filename: 'test.vue',
      code:
        '<template><div aria-roledescription="\'test some other string\'">{{ "COOL" }}></div></template>',
      errors: ['Attribute value should be marked for translation']
    },
    {
      options: [{ attributes: ['title'] }],
      filename: 'test.vue',
      code:
        '<template><p title="Hypertext Markup Language">{{ __("HTML") }}</p></template>',
      errors: ['Attribute value should be marked for translation']
    }
  ]
})
