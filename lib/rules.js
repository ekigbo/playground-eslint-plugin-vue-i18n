/** DON'T EDIT THIS FILE; was created by scripts. */
'use strict'

module.exports = {
  'no-dynamic-keys': require('./rules/no-dynamic-keys'),
  'no-html-messages': require('./rules/no-html-messages'),
  'no-missing-keys': require('./rules/no-missing-keys'),
  'no-unused-keys': require('./rules/no-unused-keys'),
  'no-v-html': require('./rules/no-v-html'),
  'no-bare-strings': require('./rules/no-bare-strings'),
  'no-bare-attribute-strings': require('./rules/no-bare-attribute-strings')
}
